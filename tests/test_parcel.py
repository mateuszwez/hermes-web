import pytest
from classes.parcel import Parcel
from classes.address import Address
from copy import deepcopy

street = 'Maple Street'
house_num = 22
apartment_num = 5
city = 'London'
postal_code = '22-428'
country = 'United Kingdom'


class TestParcel:
    
    def test_init(self):
        addr = Address(street, house_num, apartment_num,
                       city, postal_code, country)

        parcel = Parcel('ON_ROAD', addr, addr, addr)

        assert parcel.status == 'ON_ROAD'
        assert parcel.c_point_addr == addr
        assert parcel.client_addr == addr
        assert parcel.warehouse_addr == addr

        parcel = Parcel()

        assert parcel.status == 'IN_WAREHOUSE'
        assert parcel.c_point_addr is None
        assert parcel.client_addr is None
        assert parcel.warehouse_addr is None

    def test_setters(self):
        addr = Address(street, house_num, apartment_num,
                       city, postal_code, country)

        parcel = Parcel()

        parcel.status = 'DELIVERED'
        parcel.c_point_addr = addr
        parcel.client_addr = addr
        parcel.warehouse_addr = addr

        assert parcel.status == 'DELIVERED'
        assert parcel.c_point_addr == addr
        assert parcel.client_addr == addr
        assert parcel.warehouse_addr == addr

    def test_invalid_input(self):
        with pytest.raises(ValueError):
            parcel = Parcel('Ducks')

        parcel = Parcel()

        with pytest.raises(TypeError):
            parcel.c_point_addr = 1
        with pytest.raises(TypeError):
            parcel.client_addr = 2
        with pytest.raises(TypeError):
            parcel.warehouse_addr = 3

