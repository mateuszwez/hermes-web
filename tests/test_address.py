import pytest
from classes.address import Address

street = 'Maple Street'
house_num = 22
apartment_num = 5
city = 'London'
postal_code = '22-428'
country = 'United Kingdom'


class TestAddress:

    def test_init(self):
        addr = Address(street, house_num, apartment_num,
                       city, postal_code, country)

        assert addr.street == street
        assert addr.house_num == house_num
        assert addr.apartment_num == apartment_num
        assert addr.city == city
        assert addr.postal_code == postal_code
        assert addr.country == country

        addr = Address()

        assert addr.street is None
        assert addr.house_num is None
        assert addr.apartment_num is None
        assert addr.city is None
        assert addr.postal_code is None
        assert addr.country is None

    def test_setters(self):
        addr = Address()

        addr.street = street
        addr.house_num = house_num
        addr.apartment_num = apartment_num
        addr.city = city
        addr.postal_code = postal_code
        addr.country = country

        assert addr.street == street
        assert addr.house_num == house_num
        assert addr.apartment_num == apartment_num
        assert addr.city == city
        assert addr.postal_code == postal_code
        assert addr.country == country

    def test_invalid_input(self):
        addr = Address()

        with pytest.raises(TypeError):
            addr.street = 2
        with pytest.raises(TypeError):
            addr.house_num = 'two'
        with pytest.raises(TypeError):
            addr.apartment_num = 'two'
        with pytest.raises(TypeError):
            addr.city = 2
        with pytest.raises(TypeError):
            addr.postal_code = 2
        with pytest.raises(TypeError):
            addr.country = 2
