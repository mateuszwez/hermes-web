import pytest
import cx_Oracle
import database as db
from classes.address import Address
from classes.parcel import Parcel


class TestLoadParcel:

    def test_good_code(self):
        assert db.load_parcel(123456789) is not None

    def test_wrong_code(self):
        assert db.load_parcel(1) is None

    def test_invalid_argument_type(self):
        with pytest.raises(TypeError):
            db.load_parcel('ducks')


class TestGetAddress:

    def test_good_id(self):
        addr = Address()

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            addr = db.get_address(connection, 5)

        assert addr.street == 'Mraz Divide'
        assert addr.house_num == 33
        assert addr.apartment_num == 33
        assert addr.city == 'Krakow'
        assert addr.postal_code == '11444'
        assert addr.country == 'Reunion'

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            addr = db.get_address(connection, 1, client_address=False)

        assert addr.street == 'Walker Roads'
        assert addr.house_num == 2154
        assert addr.apartment_num == 4309
        assert addr.city == 'Kemmermouth'
        assert addr.postal_code == '25449'
        assert addr.country == 'Iraq'

    def test_bad_id(self):
        addr = Address()

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            addr = db.get_address(connection, 1000)

        assert addr is None

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            with pytest.raises(TypeError):
                addr = db.get_address(connection, 'ducks')

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
                addr = db.get_address(connection, -1)

        assert addr is None


class TestGetParcel:

    def test_good_id(self):
        parcel = Parcel()

        # parcel on road
        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            parcel = db.get_parcel(connection, 123456789)

        assert parcel.status == 'ON_ROAD'
        assert parcel.c_point_addr is None
        assert parcel.warehouse_addr is None

        assert parcel.client_addr.street == 'Londa Junction'
        assert parcel.client_addr.house_num == 62
        assert parcel.client_addr.apartment_num == 125
        assert parcel.client_addr.city == 'West Tomifurt'
        assert parcel.client_addr.postal_code == '02479'
        assert parcel.client_addr.country == 'Iraq'

        # parcel in warehouse
        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            parcel = db.get_parcel(connection, 339062767)

        assert parcel.status == 'IN_WAREHOUSE'
        assert parcel.c_point_addr is None
        assert parcel.client_addr is not None
        
        assert parcel.warehouse_addr.street == 'Latrisha Bridge'
        assert parcel.warehouse_addr.house_num == 49583
        assert parcel.warehouse_addr.apartment_num == 99167
        assert parcel.warehouse_addr.city == 'Ezrastad'
        assert parcel.warehouse_addr.postal_code == '29176'
        assert parcel.warehouse_addr.country == 'Paraguay'

        # parcel in collection point
        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            parcel = db.get_parcel(connection, 141199250)

        assert parcel.status == 'IN_C_POINT'
        assert parcel.warehouse_addr is None
        assert parcel.client_addr is not None

        assert parcel.c_point_addr.street == 'O\'Kon Walk'
        assert parcel.c_point_addr.house_num == 28530
        assert parcel.c_point_addr.apartment_num == 57061
        assert parcel.c_point_addr.city == 'West Katherine'
        assert parcel.c_point_addr.postal_code == '52291'
        assert parcel.c_point_addr.country == 'Reunion'

    
    def test_bad_id(self):
        parcel = Parcel()

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            parcel = db.get_parcel(connection, 1)
        
        assert parcel is None

        with cx_Oracle.connect(db.LOGIN, db.PASSWORD, db.URL, encoding="UTF-8") as connection:
            with pytest.raises(TypeError):
                parcel = db.get_parcel(connection, 'ducks')
