from flask import Flask, render_template, url_for, redirect
from forms import InputForm
import database as db
import random

app = Flask(__name__)
app.config['SECRET_KEY'] = '4dd6d1e6971abc5f79ae6df3c7b85943'


@app.route('/', methods=['GET', 'POST'])
def home():
    form = InputForm()
    if form.validate_on_submit():
        code = form.parcel_code.data
        return redirect(url_for('parcel', code=code))
    return render_template('home.html', form=form)


@app.route('/parcel/<int:code>', methods=['GET', 'POST'])
def parcel(code):
    parcel = db.load_parcel(code)
    form = InputForm()

    if form.validate_on_submit():
        code = form.parcel_code.data
        return redirect(url_for('parcel', code=code))

    return render_template('parcel.html', form=form, code=code,
        parcel=parcel)


@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == "__main__":
    # don't use debug mode in app.run()!
    # somehow it doesn't work with cx_Oracle
    app.run(port=5500)
