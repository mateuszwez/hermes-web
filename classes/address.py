
class Address:

    def __init__(self, street=None, house_num=None, apartment_num=None,
                 city=None, postal_code=None, country=None):
        for arg in [street, city, postal_code, country]:
            self.__validate_string(arg)

        for arg in [house_num, apartment_num]:
            self.__validate_int(arg)

        self.__street = street
        self.__house_num = house_num
        self.__apartment_num = apartment_num
        self.__city = city
        self.__postal_code = postal_code
        self.__country = country

    @property
    def street(self):
        return self.__street

    @street.setter
    def street(self, value):
        self.__validate_string(value)
        self.__street = value

    @property
    def house_num(self):
        return self.__house_num

    @house_num.setter
    def house_num(self, value):
        self.__validate_int(value)
        self.__house_num = value

    @property
    def apartment_num(self):
        return self.__apartment_num

    @apartment_num.setter
    def apartment_num(self, value):
        self.__validate_int(value)
        self.__apartment_num = value

    @property
    def city(self):
        return self.__city

    @city.setter
    def city(self, value):
        self.__validate_string(value)
        self.__city = value

    @property
    def postal_code(self):
        return self.__postal_code

    @postal_code.setter
    def postal_code(self, value):
        self.__validate_string(value)
        self.__postal_code = value

    @property
    def country(self):
        return self.__country

    @country.setter
    def country(self, value):
        self.__validate_string(value)
        self.__country = value

    def __validate_int(self, value):
        arg_type = type(value).__name__
        if arg_type != 'int' and value is not None:
            raise TypeError(f'Incorrect type for argument: {value}.')

    def __validate_string(self, value):
        arg_type = type(value).__name__
        if arg_type != 'str' and value is not None:
            raise TypeError(f'Incorrect type for argument: {value}.')
