
class Parcel:

    STATUSES = [
        'ON_ROAD',
        'IN_WAREHOUSE',
        'IN_C_POINT',
        'DELIVERED'
    ]

    def __init__(self, status='IN_WAREHOUSE', c_point_addr=None,
                 client_addr=None, warehouse_addr=None):
        self.__validate_status(status)

        for arg in [c_point_addr, client_addr, warehouse_addr]:
            self.__validate_address(arg)

        self.__status = status
        self.__c_point_addr = c_point_addr
        self.__client_addr = client_addr
        self.__warehouse_addr = warehouse_addr

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, value):
        self.__validate_status(value)
        self.__status = value

    @property
    def c_point_addr(self):
        return self.__c_point_addr

    @c_point_addr.setter
    def c_point_addr(self, value):
        self.__validate_address(value)
        self.__c_point_addr = value

    @property
    def client_addr(self):
        return self.__client_addr

    @client_addr.setter
    def client_addr(self, value):
        self.__validate_address(value)
        self.__client_addr = value

    @property
    def warehouse_addr(self):
        return self.__warehouse_addr

    @warehouse_addr.setter
    def warehouse_addr(self, value):
        self.__validate_address(value)
        self.__warehouse_addr = value

    def __validate_status(self, status):
        if status not in Parcel.STATUSES:
            raise ValueError(f'Status: {status} is invalid.')

    def __validate_address(self, addr):
        arg_type = type(addr).__name__
        if arg_type != 'Address' and addr is not None:
            raise TypeError(f'Incorrect type for argument: {addr}.')
