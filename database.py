import cx_Oracle
from classes.parcel import Parcel
from classes.address import Address

URL = 'ora4.ii.pw.edu.pl:1521/pdb1.ii.pw.edu.pl'
LOGIN = 'BD1_Z15'
PASSWORD = 'twheas'

cx_Oracle.init_oracle_client()


def get_address(connection, addr_id, client_address=True):
    if type(addr_id).__name__ != 'int':
        raise TypeError(
            f'Addr_id must be an integer. Given addr_id = {addr_id}')

    cursor = connection.cursor()
    if client_address:
        cursor.execute(
            '''select a.street, a.house_number, a.apartment_number,
            a.city, a.postal_code, c.name
            from client_addresses a
            join countries c on(a.country_id = c.countries_id)
            where client_addresses_id = :addr_id''',
            [addr_id]
        )
    else:
        cursor.execute(
            '''select a.street, a.house_number, a.apartment_number,
            a.city, a.postal_code, c.name
            from company_addresses a
            join countries c on(a.countries_id = c.countries_id)
            where comp_addr_id = :addr_id''',
            [addr_id]
        )
    row = cursor.fetchone()

    if row is None:
        return None

    addr = Address()

    addr.street = row[0]
    addr.house_num = row[1]
    addr.apartment_num = row[2]
    addr.city = row[3]
    addr.postal_code = row[4]
    addr.country = row[5]

    return addr


def get_parcel(connection, code):
    if type(code).__name__ != 'int':
        raise TypeError(
            f'Code must be an integer. Given code = {code}')

    cursor = connection.cursor()
    cursor.execute(
        '''select status, collection_point_id, department_id, receiver_id
        from parcels where code = :code_value''',
        [code]
    )
    row = cursor.fetchone()

    if row is None:
        return None

    parcel = Parcel()

    parcel.status = row[0]

    # load collection point address
    c_point_id = row[1]
    if c_point_id is not None:
        cursor.execute(
            '''select address_id from collection_points
        where collection_points_id = :c_point_id''',
            [c_point_id]
        )
        row_2 = cursor.fetchone()
        c_point_addr_id = row_2[0]
        parcel.c_point_addr = get_address(
            connection, c_point_addr_id, False)

    # load warehouse address
    warehouse_id = row[2]
    if warehouse_id is not None:
        cursor.execute(
            '''select address_id from departments
        where departments_id = :warehouse_id''',
            [warehouse_id]
        )
        row_2 = cursor.fetchone()
        warehouse_addr_id = row_2[0]
        parcel.warehouse_addr = get_address(
            connection, warehouse_addr_id, False)

    # load client address
    client_id = row[3]
    if client_id is not None:
        cursor.execute(
            '''select address_id from clients
        where clients_id = :client_id''',
            [client_id]
        )
        row_2 = cursor.fetchone()
        client_addr_id = row_2[0]
        parcel.client_addr = get_address(connection, client_addr_id)

    return parcel


def load_parcel(code):
    '''This function takes parcel code (must be an integer) and returns
    parcel object (see classes/parcel.py).
    If no parcel was found with given code function returns None.
    '''

    if type(code).__name__ != 'int':
        raise TypeError(f'Code must be an integer! Given code = {code}')

    with cx_Oracle.connect(LOGIN, PASSWORD, URL, encoding="UTF-8") as connection:
        return get_parcel(connection, code)
