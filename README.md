## Opis strony
Strona ta jest przeznaczona dla klientów i umożliwia intuicyjny oraz łatwy przegląd przesyłek oraz wyświetla informacje o firmie.

## Realizacja etapu
* stworzenie strony internetowej
  * Backend
    * Stworzenie aplikacji we Flasku
    * Napisanie testów jednostkowych do backendu

  * Frontend
    * stworzenie templatów w html
    * stworzenie strony głównej, na której znajduje się nagłówek z nazwą firmy i nawigacją, przykładowe zdjęcie, formularz, do którego użytkownik może wpisać numer ID przesyłki i dowiedzieć się jaki ona ma status i gdzie się znajduje, krótki opis osiągnięć firmy i footer, w którym wypisani zostali autorzy oraz ikonka z linkiem do gitlaba
    * stworzenie about page, na której znajdują się dodatkowe informacje o firmie oraz mail do kontaktu
    * napisanie stylów css, wykorzystanie ikonek z fontawesome, styl strony pozostał w formie minimalistycznej, zależało nam na prostocie
    * zadbanie o responsywność aplikacji z użyciem bootstrapa oraz odpowiednich stylów, strona na każdym urządzeniu będzie wyglądała dobrze
    * napisanie skryptów w javascript np. do automatycznego przewijania strony

* Połączenie strony z bazą danych
  * Połączenie strony z bazą danych, tak aby dane pobierane były z zaimplementowanej przez nas bazy stworzonej na projekt z BD1, którą również wykorzystaliśmy do projektu desktopowego w Javie. Do połączenia się z bazą został użyty sterownik cx_Oracle we flasku
  * Pobieranie informacji o paczkach z bazy danych, na podstawie ID wyszukiwana jest paczka i zwracane są odpowiednie informacje do strony, która to wyświetla użyteczne informacje dla użytkownika

* Deploy na serwerze
  * Strony nie udało się umieścić na serwerze, ponieważ występywały bardzo duże problemy z bazą danych oracla, był pomysł, aby przenieść całą bazę na np. postgresql, ale wtedy musielibyśmy zmienić bardzo dużo w aplikacji, a czasu na to niestety by nie wystarczyło 

**Możliwości strony:**
* sprawdzanie statusu zamówienia
  * przesyłka wysłana
  * w drodze
  * oczekująca na odebranie
* sprawdzanie informacji o firmie kurierskiej
  * landing page
  * about page


## Otwieranie projektu

1. Zainstalować [python 3.5](https://www.python.org/) lub nowszą wersję.

2. Zainstalować moduł [flask](https://flask.palletsprojects.com/en/1.1.x/) oraz [wtforms](https://flask-wtf.readthedocs.io/en/stable/).

        python -m pip install flask
        python -m pip install flask-wtf

3. Do testowania aplikacji użyty został moduł [pytest](https://docs.pytest.org/en/stable/).

        python -m pip install pytest

4. Przejść do katalogu projektu i uruchomić aplikację.

        python app.py

5. W terminalu wyświetli się wiadomość że serwer został uruchomiony oraz wyświetli się adres lokalny, na którym działa strona.

        * Running on http://127.0.0.1:5000/

    Aby otworzyć stronę należy skopiować ten link do przeglądarki.

## Inne informacje o projekcie

**Research:**
* Wyszukanie dostępnych frameworków do tworzenia stron:
  * [flask](https://flask.palletsprojects.com/en/1.1.x/)
  * [django](https://www.djangoproject.com/)
  * [express](https://expressjs.com/)
* Wybraliśmy flask'a ze względu na łatwość w używaniu oraz znajomość pythona. 
* Dodatkowo do tworzenia responsywnej strony będziemy używali [bootstrapa](https://getbootstrap.com/)

**Środowisko pracy i wybrane narzędzia:**
* Backend:
  * Flask (połączenie z bazą danych, którą wrzucimy na serwer)
* Frontend:
  * HTML
  * CSS
  * JavaScript
  * Bootstrap
