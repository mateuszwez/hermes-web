from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField
from wtforms.validators import DataRequired, NumberRange

MIN_INT = 0
MAX_INT = 999999999


class InputForm(FlaskForm):
    parcel_code = IntegerField('Code', validators=[DataRequired(), NumberRange(
        min=MIN_INT, max=MAX_INT)], render_kw={'placeholder': "Enter Parcel Code"})
    submit = SubmitField('Check Status')
